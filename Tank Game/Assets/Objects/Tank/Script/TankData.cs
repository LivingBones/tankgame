﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float forwardSpeed = 30.0f;
    public float rotateSpeed = 180.0f;
    public float reverseSpeed = 20.0f;
}
