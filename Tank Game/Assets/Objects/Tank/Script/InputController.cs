﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum inputScheme { arrowKey, WASD};
    public inputScheme input = inputScheme.WASD;

    private float horizontalInput;
    private float verticalInput;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (input)
        {
            case inputScheme.arrowKey:
                horizontalInput = Input.GetAxis("HorizontalArrows");
                verticalInput = Input.GetAxis("VerticalArrows");
                break;
            case inputScheme.WASD:
                horizontalInput = Input.GetAxis("HorizontalWASD");
                verticalInput = Input.GetAxis("VerticalWASD");
                break;
        }
        gameObject.SendMessage("Move", verticalInput, SendMessageOptions.DontRequireReceiver);
        gameObject.SendMessage("Rotate", horizontalInput, SendMessageOptions.DontRequireReceiver);
    }
}
