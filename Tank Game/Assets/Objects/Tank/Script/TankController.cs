﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    private CharacterController characterController;
    private Transform tf;
    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        characterController = gameObject.GetComponent<CharacterController>();
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(float speed)
    {
        if (speed < 0)
        {
            speed = speed * data.reverseSpeed;
        }
        else
        {
            speed = speed * data.forwardSpeed;
        }
        Vector3 moveVector = tf.forward * speed * Time.deltaTime;
        characterController.SimpleMove(moveVector);
    }

    public void Rotate(float speed)
    {
        Vector3 rotateVector = tf.up * speed * Time.deltaTime * data.rotateSpeed;
        tf.Rotate(rotateVector, Space.Self);
    }

}
